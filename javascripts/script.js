/* Author: Henry Holmes

*/

/* ------------------------ Vegas Background Image Activation */
// On the index page
if ($("#index").is('*')) {
	$( function() {
	  $f = 1450;
	  $.vegas('slideshow', {
	  delay:8500,
	  backgrounds:[
	    { src:'/images/dance/flying-bg.jpg', fade:$f },
	    { src:'/images/dance/aphorismicity.jpg', fade:$f },
	    { src:'/images/dance/claws.jpg', fade:$f },
	    { src:'/images/dance/tpwc.jpg', fade:$f },
	    { src:'/images/dance/pare.jpg', fade:$f },
	    { src:'/images/dance/two.jpg', fade:$f },
	  ]
	})('overlay', {
	    src:'/vegas/overlays/06.png',
	    opacity: 0.8
	  });
	});
}
;
